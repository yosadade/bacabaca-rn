import Icon from './icon.png';
import ILNews from './image20.png';
import IconNext from './ic-next.svg';
import IconEditProfile from './ic-edit-profile.svg';
import IconLanguage from './ic-language.svg';
import IconRate from './ic-rate.svg';
import IconHelp from './ic-help.svg';
import ILProfile from './me.jpeg';

export {
  Icon,
  ILNews,
  IconNext,
  IconEditProfile,
  IconLanguage,
  IconRate,
  IconHelp,
  ILProfile,
};
