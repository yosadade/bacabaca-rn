import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {colors, fonts} from '../../../utils';
import IconOnly from './IconOnly';

const Button = ({type, onPress, title, color}) => {
  if (type === 'icon-only') {
    return <IconOnly onPress={onPress} color={color} />;
  }
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.title(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: type => ({
    backgroundColor:
      type === 'secondary' ? colors.button.secondary : colors.button.primary,
    paddingVertical: 10,
    borderRadius: 10,
    justifyContent: 'center',
  }),
  title: type => ({
    fontSize: 18,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
    color: colors.text.secondary,
  }),
});
