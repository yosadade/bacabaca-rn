import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {colors, fonts} from '../../../utils';

const ListNews = ({image, title, address, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.content}>
        <Image source={{uri: image}} style={styles.picture} />
        <View style={styles.wrapperTitle}>
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>
          <Text style={styles.address}>{address}</Text>
        </View>
      </View>
      <TouchableOpacity>
        <MaterialCommunityIcons
          name="star"
          size={20}
          color={colors.icon.tertiary}
        />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

export default ListNews;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.border,
  },
  content: {
    flexDirection: 'row',
  },
  picture: {
    width: 80,
    height: 60,
    borderRadius: 11,
    marginRight: 16,
  },
  wrapperTitle: {
    justifyContent: 'flex-start',
    paddingRight: '40%',
  },
  title: {
    fontSize: 14,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  address: {
    fontSize: 12,
    fontFamily: fonts.primary[300],
    color: colors.text.senary,
    marginTop: 5,
  },
});
