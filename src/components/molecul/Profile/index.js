import React from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';
import {colors, fonts} from '../../../utils';
import {ILProfile} from '../../../assets';

const Profile = ({name, desc}) => {
  return (
    <View style={styles.container}>
      <View style={styles.borderProfile}>
        <Image source={ILProfile} style={styles.photo} />
      </View>
      <View>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.profession}>{desc}</Text>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
  borderProfile: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    borderWidth: 1,
    borderColor: colors.border,
    alignItems: 'center',
    justifyContent: 'center',
  },
  photo: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
  },
  name: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 16,
    textAlign: 'center',
  },
  profession: {
    fontSize: 16,
    fontFamily: fonts.primary[300],
    color: colors.text.senary,
    marginTop: 2,
    textAlign: 'center',
  },
});
