import Header from './Header';
import ListNews from './ListNews';
import Loading from './Loading';
import List from './List';

export {Header, ListNews, Loading, List};
