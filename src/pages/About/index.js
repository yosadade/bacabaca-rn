import React from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import Fire from '../../config';
import {colors} from '../../utils';
import {List, Gap} from '../../components';
import Profile from '../../components/molecul/Profile';
import {showMessage} from 'react-native-flash-message';

const About = ({navigation}) => {
  const signOut = () => {
    Fire.auth()
      .signOut()
      .then(() => {
        console.log('sign out');
        navigation.replace('GetStarted');
      })
      .catch(err => {
        showMessage({
          message: err.message,
          type: 'default',
          backgroundColor: colors.background.error,
          color: colors.text.secondary,
        });
      });
  };
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderContent navigation={navigation} />
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor={colors.background.primary}
      hidden={false}
    />
  );
};

const RenderContent = ({navigation}) => {
  return (
    <View style={styles.content}>
      <Gap height={14} />
      <Profile name="Yosada Dede Arissa" desc="Apps Developer" />
      <Gap height={10} />
      <List
        icon="edit-profile"
        name="About"
        desc="Bacaba Build Version 1.0.0"
        type="next"
      />
      <List
        icon="language"
        name="Programming Language"
        desc="Javascript, React Native"
        type="next"
      />
      <List
        icon="rate"
        name="Give Us Rate"
        desc="On Google Play Store"
        type="next"
      />
      <List
        icon="help"
        name="Sign Out"
        desc="Read our guideines"
        type="next"
        onPress={() => navigation.replace('GetStarted')}
      />
    </View>
  );
};

export default About;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.tertiary,
  },
  content: {
    flex: 1,
    backgroundColor: colors.background.primary,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingTop: 40,
  },
});
