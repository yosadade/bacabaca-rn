import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {colors, fonts} from '../../utils';
import {Button} from '../../components';

const ContentNews = ({route, navigation}) => {
  const {title, address, content, image} = route.params;
  console.log(image);
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderMainNews navigation={navigation} image={image} />
      <RenderContent title={title} address={address} content={content} />
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="light-content"
      backgroundColor="transparent"
      hidden={false}
      translucent
    />
  );
};

const RenderMainNews = ({image, navigation}) => {
  return (
    <ImageBackground source={{uri: image}} style={styles.background}>
      <View style={styles.overlay} />
      <View style={styles.icon}>
        <Button
          type="icon-only"
          color={colors.icon.primary}
          onPress={() => navigation.goBack()}
        />
      </View>
    </ImageBackground>
  );
};

const RenderContent = ({title, address, content}) => {
  return (
    <View style={styles.content}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text numberOfLines={2} style={styles.title}>
          {title}
        </Text>
        <Text style={styles.desc}>{content}</Text>
      </ScrollView>
    </View>
  );
};

export default ContentNews;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
  },
  icon: {
    paddingHorizontal: 20,
    paddingVertical: 40,
  },
  background: {
    height: 260,
    width: '100%',
    backgroundColor: colors.background.tertiary,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  title: {
    fontSize: 17,
    fontFamily: fonts.primary[700],
    color: colors.text.primary,
    textAlign: 'left',
    marginTop: 6,
  },
  desc: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    color: colors.text.primary,
    textAlign: 'left',
    marginTop: 12,
  },
  content: {
    flex: 1,
    backgroundColor: colors.background.primary,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginTop: -40,
    paddingVertical: 14,
    paddingHorizontal: 20,
  },
});
