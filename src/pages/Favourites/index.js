import React from 'react';
import {StyleSheet, Text, View, StatusBar} from 'react-native';
import {colors, fonts} from '../../utils';

const Favourites = () => {
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <Text style={styles.title}>Belum ada beria</Text>
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor={colors.background.primary}
      hidden={false}
    />
  );
};

export default Favourites;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
});
