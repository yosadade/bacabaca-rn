import React from 'react';
import {View, Text, StatusBar, StyleSheet, Image} from 'react-native';
import {Icon} from '../../assets';
import {Gap, Button} from '../../components';
import {colors, fonts} from '../../utils';

const GetStarted = ({navigation}) => {
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderIcon />
      <RenderButton navigation={navigation} />
    </View>
  );
};

const RenderStatusBar = () => {
  return <StatusBar barStyle="dark-content" backgroundColor="#FFFFFF" />;
};

const RenderIcon = () => {
  return (
    <View style={styles.icon}>
      <Image source={Icon} style={styles.image} />
      <Text style={styles.lead}>Baca berita jadi lebih mudah & fleksibel</Text>
    </View>
  );
};

const RenderButton = ({navigation}) => {
  return (
    <View style={styles.button}>
      <Button
        type="secondary"
        title="Get Started"
        onPress={() => navigation.navigate('Register')}
      />
      <Gap height={16} />
      <Button title="Sign In" onPress={() => navigation.navigate('Login')} />
    </View>
  );
};

export default GetStarted;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    paddingHorizontal: 40,
    paddingVertical: 20,
    backgroundColor: colors.background.primary,
    justifyContent: 'space-around',
  },
  image: {
    width: 150,
    height: 150,
    alignSelf: 'center',
  },
  lead: {
    fontSize: 25,
    fontFamily: fonts.primary[600],
    marginTop: 31,
  },
});
