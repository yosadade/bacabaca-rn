import React, {useState} from 'react';
import {showMessage} from 'react-native-flash-message';
import {StyleSheet, Text, View, StatusBar, Image} from 'react-native';
import {colors, fonts} from '../../utils';
import {Gap, Input, Link, Button, Loading} from '../../components';
import {Icon} from '../../assets';
import Fire from '../../config/Fire';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const login = () => {
    console.log(email, password);
    setLoading(true);
    Fire.auth()
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        console.log('success', res);
        setEmail('');
        setPassword('');
        navigation.replace('MainApp');
      })
      .catch(error => {
        const errorMessage = error.message;
        setLoading(false);
        showMessage({
          message: errorMessage,
          type: 'default',
          backgroundColor: colors.background.error,
          color: colors.text.secondary,
        });
        console.log('error', error);
      });
  };

  return (
    <>
      <View style={styles.page}>
        <RenderStatusBar />
        <RenderLead />
        <RenderInput
          email={email}
          setEmail={setEmail}
          password={password}
          setPassword={setPassword}
        />
        <RenderButton navigation={navigation} login={login} />
      </View>
      {loading && <Loading />}
    </>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor={colors.background.primary}
      hidden={false}
    />
  );
};

const RenderLead = () => {
  return (
    <View style={styles.lead}>
      <Image source={Icon} style={styles.logo} />
      <View style={styles.wrapTitle}>
        <Text style={styles.title}>Masuk dan mulai membaca</Text>
      </View>
    </View>
  );
};

const RenderInput = ({email, setEmail, password, setPassword}) => {
  return (
    <View style={styles.input}>
      <Input
        label="Email Address"
        value={email}
        onChangeText={value => setEmail(value)}
      />
      <Gap height={24} />
      <Input
        label="Password"
        value={password}
        onChangeText={value => setPassword(value)}
        secureTextEntry
      />
      <Gap height={10} />
      <Link title="Forgot My Password" size={12} />
    </View>
  );
};

const RenderButton = ({navigation, login}) => {
  return (
    <View style={styles.button}>
      <Button title="Sign In" onPress={login} />
      <Gap height={10} />
      <Link
        title="Create New Account"
        size={16}
        align="center"
        onPress={() => navigation.replace('Register')}
      />
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
  },
  lead: {
    paddingHorizontal: 40,
  },
  logo: {
    width: 140,
    height: 140,
    alignSelf: 'center',
  },
  wrapTitle: {
    maxWidth: 167,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 40,
  },
  input: {
    padding: 40,
  },
  button: {
    paddingHorizontal: 40,
  },
});
