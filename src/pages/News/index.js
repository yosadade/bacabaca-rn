import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {fonts, colors} from '../../utils';
import {ListNews} from '../../components';
import {ILNews} from '../../assets';

const Hospitals = ({navigation}) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    getDataNews();
    console.log(setData);
  }, []);

  const getDataNews = () => {
    axios
      .get(
        'http://newsapi.org/v2/top-headlines?country=id&apiKey=d5f4afae53b64ff5bd65140cdcb3fee4',
      )
      .then(res => {
        // console.log('success', res.data.articles);
        setData(res.data.articles);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderMainNews data={data} navigation={navigation} />
      <RenderContent data={data} navigation={navigation} />
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="light-content"
      backgroundColor="transparent"
      translucent
      hidden={false}
    />
  );
};

const RenderMainNews = ({data, navigation}) => {
  console.log(data[0]);
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('ContentNews', data[0])}>
      <ImageBackground source={ILNews} style={styles.background}>
        <View style={styles.overlay} />
        <View style={styles.wrapperTitle}>
          <Text numberOfLines={2} style={styles.title}>
            Singapura, negara dengan fasilitas sanitasi tebaik se Asia Tenggara
          </Text>
          <Text style={styles.desc}>CNN</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const RenderContent = ({navigation, data}) => {
  return (
    <View style={styles.content}>
      <FlatList
        data={data}
        renderItem={({item}) => (
          <ListNews
            image={item.urlToImage}
            title={item.title}
            address={item.source.name}
            onPress={() =>
              navigation.navigate('ContentNews', {
                image: item.urlToImage,
                title: item.title,
                content: item.content,
              })
            }
          />
        )}
        keyExtractor={(item, index) => item + index.toString()}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

export default Hospitals;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.tertiary,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  background: {
    height: 260,
    paddingTop: 30,
  },
  wrapperTitle: {
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.secondary,
    textAlign: 'center',
    marginTop: 6,
  },
  desc: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    color: colors.text.secondary,
    textAlign: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: colors.background.primary,
    // borderTopEndRadius: 20,
    // borderTopStartRadius: 20,
    borderRadius: 20,
    marginTop: -40,
    paddingTop: 14,
  },
});
