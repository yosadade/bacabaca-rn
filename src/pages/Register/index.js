import React, {useState} from 'react';
import {showMessage} from 'react-native-flash-message';
import {StyleSheet, View, StatusBar, ScrollView} from 'react-native';
import {colors} from '../../utils';
import {Header, Input, Gap, Button, Loading} from '../../components';
import Fire from '../../config/Fire';

const Register = ({navigation}) => {
  const [fullName, setFullName] = useState('');
  const [profession, setProfession] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const onContinue = () => {
    console.log(fullName, profession, email, password);
    setLoading(true);
    Fire.auth()
      .createUserWithEmailAndPassword(email, password)
      .then(success => {
        setLoading(false);
        setFullName('');
        setProfession('');
        setEmail('');
        setPassword('');
        console.log('register success', success);
        navigation.navigate('Login');
      })
      .catch(error => {
        const errorMessage = error.message;
        setLoading(false);
        showMessage({
          message: errorMessage,
          type: 'default',
          backgroundColor: colors.background.error,
          color: colors.text.secondary,
        });
      });
  };

  return (
    <>
      <View style={styles.page}>
        <RenderStatusBar />
        <RenderHeader navigation={navigation} />
        <RenderInput
          fullName={fullName}
          setFullName={setFullName}
          profession={profession}
          setProfession={setProfession}
          email={email}
          setEmail={setEmail}
          password={password}
          setPassword={setPassword}
        />
        <RenderButton navigation={navigation} onContinue={onContinue} />
      </View>
      {loading && <Loading />}
    </>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor={colors.background.primary}
      hidden={false}
    />
  );
};

const RenderHeader = ({navigation}) => {
  return <Header title="Daftar Akun" onPress={() => navigation.goBack()} />;
};

const RenderInput = ({
  fullName,
  profession,
  email,
  password,
  setFullName,
  setProfession,
  setEmail,
  setPassword,
}) => {
  return (
    <View style={styles.input}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Input
          label="Full Name"
          value={fullName}
          onChangeText={value => setFullName(value)}
        />
        <Gap height={24} />
        <Input
          label="Pekerjaan"
          value={profession}
          onChangeText={value => setProfession(value)}
        />
        <Gap height={24} />
        <Input
          label="Email Address"
          value={email}
          onChangeText={value => setEmail(value)}
        />
        <Gap height={24} />
        <Input
          label="Password"
          value={password}
          onChangeText={value => setPassword(value)}
          secureTextEntry
        />
      </ScrollView>
    </View>
  );
};

const RenderButton = ({navigation, onContinue}) => {
  return (
    <View style={styles.button}>
      <Button title="Continue" onPress={onContinue} />
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
  },
  input: {
    padding: 40,
  },
  button: {
    paddingHorizontal: 40,
  },
});
