import React, {useEffect} from 'react';
import {StyleSheet, View, Image, StatusBar} from 'react-native';
import {Icon} from '../../assets';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('GetStarted');
    }, 3000);
  });
  return (
    <View style={styles.page}>
      <RenderStatusBar />
      <RenderIcon />
    </View>
  );
};

const RenderStatusBar = () => {
  return (
    <StatusBar
      barStyle="dark-content"
      backgroundColor="#FFFFFF"
      hidden={true}
    />
  );
};

const RenderIcon = () => {
  return <Image source={Icon} style={styles.icon} />;
};

export default Splash;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {},
});
