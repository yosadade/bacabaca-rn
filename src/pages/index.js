import Splash from './Splash';
import GetStarted from './GetStarted';
import Login from './Login';
import Register from './Register';
import News from './News';
import Favourites from './Favourites';
import About from './About';
import ContentNews from './ContentNews';

export {
  Splash,
  GetStarted,
  Login,
  Register,
  News,
  Favourites,
  About,
  ContentNews,
};
