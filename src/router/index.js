import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  Splash,
  GetStarted,
  Login,
  Register,
  News,
  Favourites,
  About,
  ContentNews,
} from '../pages/';
import {colors} from '../utils';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#0BCAD4',
        style: {
          height: 60,
          backgroundColor: colors.background.tertiary,
          borderWidth: 0,
        },
        labelStyle: {
          fontSize: 12,
          marginBottom: 10,
          fontWeight: 'bold',
        },
        tabStyle: {
          marginTop: 10,
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          if (route.name === 'News') {
            return (
              <MaterialCommunityIcons
                name="newspaper"
                size={size}
                color={color}
              />
            );
          } else if (route.name === 'Favourites') {
            return (
              <MaterialCommunityIcons name="star" size={size} color={color} />
            );
          } else if (route.name === 'About') {
            return (
              <FontAwesome5 name="user-circle" size={size} color={color} />
            );
          }
        },
      })}>
      <Tab.Screen name="News" component={News} />
      <Tab.Screen name="Favourites" component={Favourites} />
      <Tab.Screen name="About" component={About} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="GetStarted" component={GetStarted} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="MainApp" component={MainApp} />
      <Stack.Screen name="ContentNews" component={ContentNews} />
    </Stack.Navigator>
  );
};

export default Router;
